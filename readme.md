Lumen

Any code without Copyright notice is free for anyone to use.

Many years ago I wrote a voxel engine called Geek.  You can see an old archived image of it here: http://www.flipcode.com/archives/03-17-2001.shtml

I decided to create something similar, just for fun, and to learn a bit more about modern open GL and various voxel techniques.

I will try to document things better then they currently are, but no guarantees.

