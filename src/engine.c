#include "engine.h"
#include "camera.h"
#include "voxel.h"
#include <GLFW/glfw3.h>

// The engine will track the frustum
// It will queue blocks to generate, and remember they are queued
// Will sort queue and generate most relevent
// Will track edits and place them into queue as well
// Will support multiple threads


size_t g_count = 0;
struct frustum f;

struct engine_thread_data {
	size_t tid;
	struct transfer_list *in;
	struct transfer_list *out;
	struct vector3i *center;
};

struct vector3i *center = NULL;

int vector3i_camera_cmp(const void *a, const void *b) {
	if ( a == b) return 0;
	if (a==NULL || b==NULL) {
		if (a==NULL) return -1;
		if (b==NULL) return 1;
	}
	const struct vector3i *c_a = a;
	const struct vector3i *c_b = b;

	float d_a = -vector3i_distance(c_a, center);
	float d_b = -vector3i_distance(c_b, center);

	if (!frustum_CubeInFrustum(&f, c_a->x-CHUNK_SIZE, c_a->y-CHUNK_SIZE, c_a->z-CHUNK_SIZE, CHUNK_SIZE*2)) d_a*=4;
	if (!frustum_CubeInFrustum(&f, c_b->x-CHUNK_SIZE, c_b->y-CHUNK_SIZE, c_b->z-CHUNK_SIZE, CHUNK_SIZE*2)) d_b*=4;

	return (int)(d_b - d_a);
}


//KHASH_MAP_INIT_STR(vector3i,struct block *);
void* engine_thread_proc(void* arg)
{
	struct engine_thread_data *etd =(struct engine_thread_data*)arg;
	printf("Starting thread id=%lu\n", etd->tid);

	while (true) {
		size_t read_amount = 0;
		struct transfer_cmd *items = NULL;
		transfer_list_lock(etd->in);
		transfer_list_read(etd->in, &read_amount, &items);
		transfer_list_unlock(etd->in);

		//if (read_amount>0) printf("TID %lu  Processing %lu (%lu)\n", etd->tid, read_amount, g_count);

		for (size_t t=0; t!= read_amount; t++) {
			struct transfer_cmd cmd = items[t];

			if (cmd.type == 0) { // create
				const struct vector3i *coord = cmd.data;

				// Generate mesh
				struct vector3i block_coord={coord->x+CHUNK_SIZE+2,coord->y+CHUNK_SIZE+2,coord->z+CHUNK_SIZE+2};

				struct block* b = malloc(sizeof(struct block));
				b->state = 0;
				b->coord = *coord;
				b->mesh = voxel_generate_NEW(coord,&block_coord);

				// Send to that thread for processing (or culling)
				struct transfer_cmd cmd = { 1, b };
				transfer_list_lock(etd->out);
				transfer_list_add(etd->out, &cmd);
				transfer_list_unlock(etd->out);
			}

			free(cmd.data);
		}
		free(items);

	}
}



KHASH_MAP_INIT_INT64(vector3i_to_process,int);
khash_t(vector3i_to_process) *vectors_to_process;

void engine_init(struct engine *e)
{
	vectors_to_process = kh_init(vector3i_to_process);

	memset(e, 0, sizeof(struct engine));

	e->thread_count = THREAD_COUNT;
	for (size_t t=0; t != e->thread_count; t++) {
		transfer_list_init(&e->transfer_to_worker_threads[t]);
	}
	transfer_list_init(&e->transfer_from_worker_threads);

	center = &e->center;
	e->center.x = -1;
	e->center.y = 2;
	e->center.z = -3;

	// Start threads to process
	for (size_t t=0; t != e->thread_count;t++) {
		pthread_t tid;
		pthread_attr_t thr_attr;
		pthread_attr_init(&thr_attr);
		pthread_attr_setdetachstate(&thr_attr, PTHREAD_CREATE_DETACHED);

		struct engine_thread_data* etd = malloc(sizeof(struct engine_thread_data));

		etd->tid = t;
		etd->in = &e->transfer_to_worker_threads[t];
		etd->out = &e->transfer_from_worker_threads;
		etd->center = &e->center;

		pthread_create(&tid, &thr_attr, engine_thread_proc, etd);
	}


}

uint32_t _engine_thread_id(const struct engine* e, const struct vector3i *coord) {
	size_t tid = (coord->x/CHUNK_SIZE*3)+ (coord->y/CHUNK_SIZE*7)+(coord->z/CHUNK_SIZE*11);
	tid%=e->thread_count;
	return tid;
}

int _calc_thread_id(struct engine* e, const struct vector3i *coord) {
	size_t tid;
	size_t min = 10000;
	for (size_t t = 0; t<e->thread_count; t++) {
		if (e->transfer_to_worker_threads[t].item_count < min) {
			min = e->transfer_to_worker_threads[t].item_count;
			tid = t;
		}
	}
	if (min != 10000) return tid;
	// Figure out which thread is processing
	return rand() % e->thread_count;
}

void _engine_enqueue_lock(struct engine *e, size_t tid) {
	pthread_mutex_lock(&e->transfer_to_worker_threads[tid].lock);
}
void _engine_enqueue_unlock(struct engine *e, size_t tid) {
	pthread_mutex_unlock(&e->transfer_to_worker_threads[tid].lock);
}

void _engine_enqueue(struct engine* e, size_t tid, const struct vector3i *coord) {
	//size_t tid = _calc_thread_id(e,coord);

	// Send to that thread for processing (or culling)
	struct vector3i *temp = malloc(sizeof(struct vector3i));
	*temp = *coord;
	struct transfer_cmd cmd = { 0, temp };
	transfer_list_add(&e->transfer_to_worker_threads[tid], &cmd);
}

bool _valid(const struct frustum *frustum, const struct vector3i *center, const struct vector3i *coord) {
	return
		vector3i_distance(center, coord) < VIEW_DISTANCE;

	//return
		//vector3i_distance(center, coord) < MIN_DISTANCE ||
		//frustum_CubeInFrustum(frustum, coord->x-CHUNK_SIZE, coord->y-CHUNK_SIZE, coord->z-CHUNK_SIZE, CHUNK_SIZE*2);
	//return true;
}

uint64_t _hash(const struct vector3i *coord) {
	return
		(((uint64_t)coord->x/CHUNK_SIZE) << 32) +
		(((uint64_t)coord->y/CHUNK_SIZE) << 16) +
		((uint64_t)coord->z/CHUNK_SIZE);
}

size_t to_process = 0;
float start = 0;
size_t tproc = 0;
size_t engine_tick(struct engine* e, struct camera *c)
{
	size_t triangles = 0;
	struct vector3i center = { (int)c->camera_pos.x, (int)c->camera_pos.y, (int)c->camera_pos.z };
	center.x /= CHUNK_SIZE; center.y /= CHUNK_SIZE; center.z /= CHUNK_SIZE;
	center.x *= CHUNK_SIZE; center.y *= CHUNK_SIZE; center.z *= CHUNK_SIZE;

	e->center = center;

	frustum_Extract(&f);

	if (memcmp(&f, &e->frustum, sizeof(struct frustum)) != 0) {
		memcpy(&e->frustum, &f, sizeof(struct frustum));

		// Find blocks in or near frustum we are interested in
		size_t coord_count = 0;
		const int s = VIEW_DISTANCE/CHUNK_SIZE;
		struct vector3i coords[ ((s*2)+1) * ((s*2)+1) * ((s*2)+1) ];
		for (int32_t x = -s; x<= s; x++) {
			for (int32_t y = -s; y<= s; y++) {
				for (int32_t z = -s; z<= s; z++) {
					struct vector3i offset = { x*CHUNK_SIZE, y*CHUNK_SIZE, z*CHUNK_SIZE };
					struct vector3i coord = vector3i_add(&offset, &center);

					const uint64_t hash = _hash(&coord);

					khiter_t k = kh_get(vector3i_to_process, vectors_to_process, hash);
					bool is_missing = (k == kh_end(vectors_to_process));
					if (is_missing) {
						if (_valid(&e->frustum, &e->center, &coord)) {
							int ret;
							k = kh_put(vector3i_to_process, vectors_to_process, hash, &ret);
							kh_value(vectors_to_process, k) = 1;

							coords[coord_count++] = coord;
						}
					}
				}
			}
		}

		e->to_generate_list = realloc(e->to_generate_list, (e->to_generate_count+coord_count)*sizeof(struct vector3i));
		memcpy(e->to_generate_list+e->to_generate_count, coords, sizeof(struct vector3i)*coord_count);
		e->to_generate_count += coord_count;
	}

	// Sort list
	g_count = e->to_generate_count;
	qsort(e->to_generate_list, e->to_generate_count, sizeof(struct vector3i), vector3i_camera_cmp);

	size_t ii = 0;
	for (size_t t = 0; t<e->thread_count; t++) {
		ii += e->transfer_to_worker_threads[t].item_count;
	}
	const size_t max_count = e->thread_count*5;
	if (ii < max_count) {
		size_t proc = 0;

		for (size_t t = 0; t < e->thread_count; t++) _engine_enqueue_lock(e, t);
		for (size_t t = 0; t < e->to_generate_count ; t++) {
			const struct vector3i coord = e->to_generate_list[t];
			if (_valid(&e->frustum, &e->center, &coord)) {
				size_t tid = _calc_thread_id(e, &coord);
				_engine_enqueue(e, tid, &coord);
				ii++;
				if (ii>=max_count) break;
			} else {
				const uint64_t hash = _hash(&coord);
				khiter_t k = kh_get(vector3i_to_process, vectors_to_process, hash);
				kh_del(vector3i_to_process, vectors_to_process, k);
			}
			proc++;
		}
		for (size_t t = 0; t < e->thread_count; t++) _engine_enqueue_unlock(e, t);


		e->to_generate_count -= proc;
		if (e->to_generate_count > 0)  {
			memmove(e->to_generate_list, e->to_generate_list+proc, sizeof(struct vector3i)*e->to_generate_count);
		}
	}

	// Do a read of any new mesh data
	size_t read_amount = 0;
	struct transfer_cmd *items = NULL;
	transfer_list_lock(&e->transfer_from_worker_threads);
	transfer_list_read(&e->transfer_from_worker_threads, &read_amount, &items);
	transfer_list_unlock(&e->transfer_from_worker_threads);


	e->block_count+=read_amount;
	e->blocks = realloc(e->blocks, sizeof(struct block)*e->block_count);
	for (size_t t=0; t!= read_amount; t++) {
		// Add to our todraw list
		struct block *b = (struct block*)items[t].data;
		tproc += b->mesh->triangle_num;
		e->blocks[e->block_count-read_amount+t] = *b;
	}
	free(items);

	to_process += read_amount;
	float cur = glfwGetTime();
	float elapsed = cur - start;
	if ( elapsed > 5) {
		printf("Proc'd: %'.2f    TProc: %'.2f\n", to_process/elapsed, tproc/elapsed);
		to_process = 0;
		start = cur;
		tproc = 0;
	}

	size_t to_delete = 0;
	for (size_t t=0; t!= e->block_count; t++) {
		struct vector3i i = e->blocks[t].coord;
		if (frustum_CubeInFrustum(&f, i.x, i.y, i.z, CHUNK_SIZE+1 )) {
			triangles += mesh_draw(e->blocks[t].mesh);
		} else {
			float d = vector3i_distance(&center, &i);
			if (d > VIEW_DISTANCE*1.5) {
				mesh_erase(e->blocks[t].mesh);
				e->blocks[t].mesh = NULL;
				to_delete++;

				const uint64_t hash = _hash(&i);
				khiter_t k = kh_get(vector3i_to_process, vectors_to_process, hash);
				kh_del(vector3i_to_process, vectors_to_process, k);
			}
		}
	}

	size_t w = 0;
	for (size_t r=0; r!= e->block_count; r++) {
		if (e->blocks[r].mesh != NULL) {
			e->blocks[w++] = e->blocks[r];
		}
	}
	e->block_count -= to_delete;

	return triangles;
}
