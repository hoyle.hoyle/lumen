#include "frustum.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

//http://www.crownandcutlass.com/features/technicaldetails/frustum.html

void frustum_Extract(struct frustum *f)
{
   float   proj[16];
   float   modl[16];
   float   clip[16];
   float   t;

   /* Get the current PROJECTION matrix from OpenGL */
   glGetFloatv( GL_PROJECTION_MATRIX, proj );

   /* Get the current MODELVIEW matrix from OpenGL */
   glGetFloatv( GL_MODELVIEW_MATRIX, modl );

   /* Combine the two matrices (multiply projection by modelview) */
   clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
   clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
   clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
   clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

   clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
   clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
   clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
   clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

   clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
   clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
   clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
   clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

   clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
   clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
   clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
   clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

   /* Extract the numbers for the RIGHT plane */
   f->planes[0][0] = clip[ 3] - clip[ 0];
   f->planes[0][1] = clip[ 7] - clip[ 4];
   f->planes[0][2] = clip[11] - clip[ 8];
   f->planes[0][3] = clip[15] - clip[12];

   /* Normalize the result */
   t = sqrt( f->planes[0][0] * f->planes[0][0] + f->planes[0][1] * f->planes[0][1] + f->planes[0][2] * f->planes[0][2] );
   f->planes[0][0] /= t;
   f->planes[0][1] /= t;
   f->planes[0][2] /= t;
   f->planes[0][3] /= t;

   /* Extract the numbers for the LEFT plane */
   f->planes[1][0] = clip[ 3] + clip[ 0];
   f->planes[1][1] = clip[ 7] + clip[ 4];
   f->planes[1][2] = clip[11] + clip[ 8];
   f->planes[1][3] = clip[15] + clip[12];

   /* Normalize the result */
   t = sqrt( f->planes[1][0] * f->planes[1][0] + f->planes[1][1] * f->planes[1][1] + f->planes[1][2] * f->planes[1][2] );
   f->planes[1][0] /= t;
   f->planes[1][1] /= t;
   f->planes[1][2] /= t;
   f->planes[1][3] /= t;

   /* Extract the BOTTOM plane */
   f->planes[2][0] = clip[ 3] + clip[ 1];
   f->planes[2][1] = clip[ 7] + clip[ 5];
   f->planes[2][2] = clip[11] + clip[ 9];
   f->planes[2][3] = clip[15] + clip[13];

   /* Normalize the result */
   t = sqrt( f->planes[2][0] * f->planes[2][0] + f->planes[2][1] * f->planes[2][1] + f->planes[2][2] * f->planes[2][2] );
   f->planes[2][0] /= t;
   f->planes[2][1] /= t;
   f->planes[2][2] /= t;
   f->planes[2][3] /= t;

   /* Extract the TOP plane */
   f->planes[3][0] = clip[ 3] - clip[ 1];
   f->planes[3][1] = clip[ 7] - clip[ 5];
   f->planes[3][2] = clip[11] - clip[ 9];
   f->planes[3][3] = clip[15] - clip[13];

   /* Normalize the result */
   t = sqrt( f->planes[3][0] * f->planes[3][0] + f->planes[3][1] * f->planes[3][1] + f->planes[3][2] * f->planes[3][2] );
   f->planes[3][0] /= t;
   f->planes[3][1] /= t;
   f->planes[3][2] /= t;
   f->planes[3][3] /= t;

   /* Extract the FAR plane */
   f->planes[4][0] = clip[ 3] - clip[ 2];
   f->planes[4][1] = clip[ 7] - clip[ 6];
   f->planes[4][2] = clip[11] - clip[10];
   f->planes[4][3] = clip[15] - clip[14];

   /* Normalize the result */
   t = sqrt( f->planes[4][0] * f->planes[4][0] + f->planes[4][1] * f->planes[4][1] + f->planes[4][2] * f->planes[4][2] );
   f->planes[4][0] /= t;
   f->planes[4][1] /= t;
   f->planes[4][2] /= t;
   f->planes[4][3] /= t;

   /* Extract the NEAR plane */
   f->planes[5][0] = clip[ 3] + clip[ 2];
   f->planes[5][1] = clip[ 7] + clip[ 6];
   f->planes[5][2] = clip[11] + clip[10];
   f->planes[5][3] = clip[15] + clip[14];

   /* Normalize the result */
   t = sqrt( f->planes[5][0] * f->planes[5][0] + f->planes[5][1] * f->planes[5][1] + f->planes[5][2] * f->planes[5][2] );
   f->planes[5][0] /= t;
   f->planes[5][1] /= t;
   f->planes[5][2] /= t;
   f->planes[5][3] /= t;
}

bool frustum_PointInFrustum( const struct frustum *f, float x, float y, float z )
{
   int p;

   for( p = 0; p < 6; p++ )
      if( f->planes[p][0] * x + f->planes[p][1] * y + f->planes[p][2] * z + f->planes[p][3] <= 0 )
         return false;
   return true;
}

bool frustum_SphereInFrustum(const struct frustum *f,  float x, float y, float z, float radius )
{
   int p;

   for( p = 0; p < 6; p++ )
      if( f->planes[p][0] * x + f->planes[p][1] * y + f->planes[p][2] * z + f->planes[p][3] <= -radius )
         return false;
   return true;
}

float frustum_SphereInFrustum2(const struct frustum *f,  float x, float y, float z, float radius )
{
   int p;
   float d;

   for( p = 0; p < 6; p++ )
   {
      d = f->planes[p][0] * x + f->planes[p][1] * y + f->planes[p][2] * z + f->planes[p][3];
      if( d <= -radius )
         return 0;
   }
   return d + radius;
}

bool frustum_CubeInFrustum(const struct frustum *f,  float x, float y, float z, float size )
{
   int p;

   for( p = 0; p < 6; p++ )
   {
      if( f->planes[p][0] * (x - size) + f->planes[p][1] * (y - size) + f->planes[p][2] * (z - size) + f->planes[p][3] > 0 )
         continue;
      if( f->planes[p][0] * (x + size) + f->planes[p][1] * (y - size) + f->planes[p][2] * (z - size) + f->planes[p][3] > 0 )
         continue;
      if( f->planes[p][0] * (x - size) + f->planes[p][1] * (y + size) + f->planes[p][2] * (z - size) + f->planes[p][3] > 0 )
         continue;
      if( f->planes[p][0] * (x + size) + f->planes[p][1] * (y + size) + f->planes[p][2] * (z - size) + f->planes[p][3] > 0 )
         continue;
      if( f->planes[p][0] * (x - size) + f->planes[p][1] * (y - size) + f->planes[p][2] * (z + size) + f->planes[p][3] > 0 )
         continue;
      if( f->planes[p][0] * (x + size) + f->planes[p][1] * (y - size) + f->planes[p][2] * (z + size) + f->planes[p][3] > 0 )
         continue;
      if( f->planes[p][0] * (x - size) + f->planes[p][1] * (y + size) + f->planes[p][2] * (z + size) + f->planes[p][3] > 0 )
         continue;
      if( f->planes[p][0] * (x + size) + f->planes[p][1] * (y + size) + f->planes[p][2] * (z + size) + f->planes[p][3] > 0 )
         continue;
      return false;
   }
   return true;
}
