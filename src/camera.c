#include "camera.h"
#include "main.h"
#include "mesh.h"
#include <OpenGL/glu.h>

////////////////////////Quaternion and Vector Code////////////////////////

////////////////////////END Quaternion and Vector Code////////////////////////

struct camera* camera_create(struct vector3f *pos, struct vector3f *lookat, struct vector3f *up, float viewscale)
{
	struct camera *c = malloc(sizeof(struct camera));
	c->max_pitch_rate = 5;
	c->max_heading_rate = 5;
	c->camera_pos = *pos;
	c->look_at = *lookat;
	c->camera_up = *up;
	c->camera_heading = 0;
	c->camera_pitch = 0;
	c->dir = (struct vector3f) { 0, 0, 1 };
	c->mouse_pos = (struct vector3f) { 0,0,0 };
	c->camera_pos_delta = (struct vector3f) { 0,0,0 };
	c->scale = viewscale;

	return c;
}

void camera_ChangePitch(struct camera* c, float degrees) {
	if (fabs(degrees) < fabs(c->max_pitch_rate)) {
		c->camera_pitch += degrees;
	} else {
		if (degrees < 0) {
			c->camera_pitch -= c->max_pitch_rate;
		} else {
			c->camera_pitch += c->max_pitch_rate;
		}
	}

	if (c->camera_pitch > 360.0f) {
		c->camera_pitch -= 360.0f;
	} else if (c->camera_pitch < -360.0f) {
		c->camera_pitch += 360.0f;
	}
}

void camera_ChangeHeading(struct camera* c, float degrees) {
	if (fabs(degrees) < fabs(c->max_heading_rate)) {
		if ((c->camera_pitch > 90 && c->camera_pitch < 270) || (c->camera_pitch < -90 && c->camera_pitch > -270)) {
			c->camera_heading -= degrees;
		} else {
			c->camera_heading += degrees;
		}
	} else {
		if (degrees < 0) {
			if ((c->camera_pitch > 90 && c->camera_pitch < 270) || (c->camera_pitch < -90 && c->camera_pitch > -270)) {
				c->camera_heading += c->max_heading_rate;
			} else {
				c->camera_heading -= c->max_heading_rate;
			}
		} else {
			if ((c->camera_pitch > 90 && c->camera_pitch < 270) || (c->camera_pitch < -90 && c->camera_pitch > -270)) {
				c->camera_heading -= c->max_heading_rate;
			} else {
				c->camera_heading += c->max_heading_rate;
			}
		}
	}

	if (c->camera_heading > 360.0f) {
		c->camera_heading -= 360.0f;
	} else if (c->camera_heading < -360.0f) {
		c->camera_heading += 360.0f;
	}
}
void camera_Move2D(struct camera* c, int x, int y) {
	struct vector3f mouse_delta = vector3f_subtract(&c->mouse_pos , &(struct vector3f){x, y, 0});
	camera_ChangeHeading(c, .02 * mouse_delta.x);
	camera_ChangePitch(c, .02 * mouse_delta.y);
	c->mouse_pos = (struct vector3f){x, y, 0};
}
void camera_SetPos(struct camera* c, int button, int state, int x, int y) {
	c->mouse_pos = (struct vector3f){x, y, 0};
}
void camera_Update(struct camera* c) {
	struct vector4f pitch_quat, heading_quat;
	struct vector3f angle;
	angle = vector3f_cross(&c->dir, &c->camera_up);
	pitch_quat = Q_from_AngAxis(c->camera_pitch, &angle);
	heading_quat = Q_from_AngAxis(c->camera_heading, &c->camera_up);
	struct vector4f temp = vector4f_mod(&pitch_quat , &heading_quat);
	temp = vector4f_normalize(&temp);
	c->dir = quatRotate(&c->dir, &temp);
	vector3f_add_assign(&c->camera_pos, &c->camera_pos_delta);
	c->look_at = vector3f_add(&c->camera_pos , &c->dir);
	c->camera_heading *= .5;
	c->camera_pitch *= .5;
	c->camera_pos_delta = vector3f_multiply_value(&c->camera_pos_delta , .5);
	gluLookAt(c->camera_pos.x, c->camera_pos.y, c->camera_pos.z, c->look_at.x, c->look_at.y, c->look_at.z, c->camera_up.x, c->camera_up.y, c->camera_up.z);
}
void camera_Forward(struct camera* c) {
	struct vector3f temp =  vector3f_multiply_value(&c->dir , c->scale);
	vector3f_add_assign(&c->camera_pos_delta , &temp);
}
void camera_Back(struct camera* c) {
	struct vector3f temp = vector3f_multiply_value(&c->dir , -c->scale);
	vector3f_add_assign(&c->camera_pos_delta , &temp);
}
void camera_Right(struct camera* c) {
	struct vector3f cross =  vector3f_cross(&c->dir, &c->camera_up) ;
	struct vector3f temp = vector3f_multiply_value( &cross , c->scale);
	vector3f_add_assign(&c->camera_pos_delta , &temp);
}
void camera_Left(struct camera* c) {
	struct vector3f cross =  vector3f_cross(&c->dir, &c->camera_up) ;
	struct vector3f temp = vector3f_multiply_value( &cross , -c->scale);
	vector3f_add_assign(&c->camera_pos_delta , &temp);
}
void camera_Up(struct camera* c) {
	struct vector3f temp =  vector3f_multiply_value(&c->camera_up , c->scale);
	vector3f_add_assign(&c->camera_pos_delta , &temp);
}
void camera_Down(struct camera* c) {
	struct vector3f temp =  vector3f_multiply_value(&c->camera_up , -c->scale);
	vector3f_add_assign(&c->camera_pos_delta , &temp);
}
