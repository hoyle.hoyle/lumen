#include "vector.h"

float vector3f_distance(const struct vector3f *a, const struct vector3f *b) {
	float x = b->x - a->x;
	float y = b->y - a->y;
	float z = b->z - a->z;
	return sqrt( (x*x) + (y*y) + (z*z) );
}

float vector3i_distance(const struct vector3i *a, const struct vector3i *b) {
	int32_t x = b->x - a->x;
	int32_t y = b->y - a->y;
	int32_t z = b->z - a->z;
	return sqrt( (x*x) + (y*y) + (z*z) );
}

bool vector3i_equal(const struct vector3i* a, const struct vector3i* b)
{
	if (a==b) return true;
	if (a==NULL || b==NULL) return false;

	return a->x == b->x && a->y == b->y && a->z == b->z;
}

bool _color4c_equal(const struct color4c *a, const struct color4c* b)
{
	if (a==b) return true;
	if (a==NULL || b==NULL) return false;

	return a->r == b->r && a->g == b->g && a->b == b->b && a->a == b->a;
}

float _color4c_compare(const struct color4c *a, const struct color4c* b)
{
	if (a==b) return 0;
	if (a==NULL || b==NULL) return NAN;

	float v;
	if ((v = a->r - b->r) != 0) return v;
	if ((v = a->g - b->g) != 0) return v;
	if ((v = a->b - b->b) != 0) return v;
	return a->a - b->a;
}

bool _vector3f_equal(const struct vector3f* a, const struct vector3f* b)
{
	if (a==b) return true;
	if (a==NULL || b==NULL) return false;

	return a->x == b->x && a->y == b->y && a->z == b->z;
}

float _vector3f_compare(const struct vector3f* a, const struct vector3f* b)
{
	if (a==b) return 0;
	if (a==NULL || b==NULL) return NAN;

	float v;
	if ((v = a->x - b->x) != 0) return v;
	if ((v = a->y - b->y) != 0) return v;
	return a->z - b->z;
}

struct vector3i vector3i_add(const struct vector3i *rhs, const struct vector3i *lhs)
{
    struct vector3i temp;
    temp.x = rhs->x + lhs->x;
    temp.y = rhs->y + lhs->y;
    temp.z = rhs->z + lhs->z;
    return temp;
}

// return rhs + lhs
struct vector3f vector3f_add(const struct vector3f *rhs, const struct vector3f *lhs)
{
    struct vector3f temp;
    temp.x = rhs->x + lhs->x;
    temp.y = rhs->y + lhs->y;
    temp.z = rhs->z + lhs->z;
    return temp;
}
// return rhs - lhs
struct vector3f vector3f_subtract(const struct vector3f *rhs, const struct vector3f *lhs)
{
    struct vector3f temp;
    temp.x = rhs->x - lhs->x;
    temp.y = rhs->y - lhs->y;
    temp.z = rhs->z - lhs->z;
    return temp;
}
// return rhs+=lhs
void vector3f_add_assign(struct vector3f *rhs, const struct vector3f *lhs)
{
	rhs->x += lhs->x;
	rhs->y += lhs->y;
	rhs->z += lhs->z;
}
// return rhs-=lhs
void vector3f_subtract_assign(struct vector3f *rhs, const struct vector3f *lhs)
{
	rhs->x -= lhs->x;
	rhs->y -= lhs->y;
	rhs->z -= lhs->z;
}

// return rhs * lhs;
struct vector3f vector3f_multiply(const struct vector3f *rhs, const struct vector3f *lhs)
{
    struct vector3f temp;
    temp.x = rhs->x * lhs->x;
    temp.y = rhs->y * lhs->y;
    temp.z = rhs->z * lhs->z;
    return temp;
}

// return rhs * lhs
struct vector3f vector3f_multiply_value(const struct vector3f *rhs, const float lhs)
{
    struct vector3f temp;
    temp.x = rhs->x * lhs;
    temp.y = rhs->y * lhs;
    temp.z = rhs->z * lhs;
    return temp;
}

struct vector3f vector3f_cross(struct vector3f *a, struct vector3f *b)
{
    struct vector3f temp = {a->y * b->z - a->z * b->y, a->z * b->x - a->x * b->z, a->x * b->y - a->y * b->x};
	return temp;
}

struct vector4f Q_from_AngAxis(float angle, struct vector3f *axis)
{
    struct vector4f quat;
    float halfang;
    float sinhalf;
    halfang = (angle * 0.5);
    sinhalf = sin(halfang);
    quat.w = cos(halfang);
    quat.x = axis->x * sinhalf;
    quat.y = axis->y * sinhalf;
    quat.z = axis->z * sinhalf;
    return (quat);
}

struct vector4f vector4f_normalize(const struct vector4f *a)
{
    float length = 1.0 / sqrt(a->w * a->w + a->x * a->x + a->y * a->y + a->z * a->z);
    struct vector4f temp = {a->w * length, a->x * length, a->y * length, a->z * length};
	return temp;
}

struct vector3f vector3f_normalize(const struct vector3f *a)
{
    float length = 1.0 / sqrt(a->x * a->x + a->y * a->y + a->z * a->z);
    struct vector3f temp = {a->x * length, a->y * length, a->z * length};
	return temp;
}


struct vector4f vector4f_inv(const struct vector4f *a)
{
    //return (1.0f / (dot(a, a))) * F4(a.x, -a.y, -a.z, -a.w);
    struct vector4f temp;
    float t1 = a->w * a->w + a->x * a->x + a->y * a->y + a->z * a->z;
    t1 = 1.0f / t1;
    temp.w = t1 * a->w;
    temp.x = -t1 * a->x;
    temp.y = -t1 * a->y;
    temp.z = -t1 * a->z;
    return temp;
}

struct vector4f vector4f_mult(const struct vector4f *a, const struct vector4f *b)
{
    struct vector4f temp;
    temp.w = a->w * b->w - a->x * b->x - a->y * b->y - a->z * b->z;
    temp.x = a->w * b->x + b->w * a->x + a->y * b->z - a->z * b->y;
    temp.y = a->w * b->y + b->w * a->y + a->z * b->x - a->x * b->z;
    temp.z = a->w * b->z + b->w * a->z + a->x * b->y - a->y * b->x;
    return temp;
}

//real4 r = mult(mult(q, real4(0, v.x, v.y, v.z)), inv(q));
//return real3(r.x, r.y, r.z);
struct vector3f quatRotate(const struct vector3f *v, const struct vector4f *q)
{
	struct vector4f temp1 = { 0, v->x, v->y, v->z };
    struct vector4f temp2 = vector4f_mult(q, &temp1);
    struct vector4f inv_q = vector4f_inv(q);
    struct vector4f r = vector4f_mult(&temp2, &inv_q);
    struct vector3f temp = {r.x, r.y, r.z};
	return temp;
}

struct vector4f vector4f_mod(const struct vector4f *rhs, const struct vector4f *lhs)
{
    return vector4f_mult(rhs, lhs);
}

bool vector3b_equal(const struct vector3b* a, const struct vector3b* b)
{
	if (a==b) return true;
	if (a==NULL || b==NULL) return false;

	return a->x == b->x && a->y == b->y && a->z == b->z;
}

float vector3b_compare(const struct vector3b* a, const struct vector3b* b)
{
	if (a==b) return 0;
	if (a==NULL || b==NULL) return NAN;

	float v;
	if ((v = a->x - b->x) != 0) return v;
	if ((v = a->y - b->y) != 0) return v;
	return a->z - b->z;
}

bool vector3s_equal(const struct vector3s* a, const struct vector3s* b)
{
	if (a==b) return true;
	if (a==NULL || b==NULL) return false;

	return a->x == b->x && a->y == b->y && a->z == b->z;
}

float vector3s_compare(const struct vector3s* a, const struct vector3s* b)
{
	if (a==b) return 0;
	if (a==NULL || b==NULL) return NAN;

	float v;
	if ((v = a->x - b->x) != 0) return v;
	if ((v = a->y - b->y) != 0) return v;
	return a->z - b->z;
}
