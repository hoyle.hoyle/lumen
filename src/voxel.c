#include "mesh.h"
#include "qef.h"
#include "simplexnoise.h"
#include "voxel.h"

#define SIZE 24
#define RSIZE 12
#define BOUNDARY 128

#define PROC2(v1,v2,a,b) \
{ \
  const uint8_t t = (((b-a)/2)+a); \
  const struct vector3f _v = _lerp_vector3f(&v1, &v2, t); \
  newPoint.x += _v.x;\
  newPoint.y += _v.y;\
  newPoint.z += _v.z;\
  num++;\
}

void print_quad(char *n, struct vector3f *va, struct vector3f *vb, struct vector3f *vc, struct vector3f *vd) {
	printf("%s: (%f,%f,%f) (%f,%f,%f) (%f,%f,%f) (%f,%f,%f)\n",
			n,
			va->x, va->y, va->z,
			vb->x, vb->y, vb->z,
			vc->x, vc->y, vc->z,
			vd->x, vd->y, vd->z);
}

bool vector_is_zero(struct vector3f *v)
{
	return v->x == 0 && v->y==0 && v->z==0;
}

void temp(struct vector3f *newPoint, struct vector3f* p, struct vector3f* n, size_t num)
{
  double matrix_A[12][3];
  double vector_B[12];
  int rows = 0;

  for (size_t t = 0; t != num; t++) {
    // px,py,pz  is the intersection point
    // nx,ny,nz  is the normal vector at that point
    const float px = p[t].x;
    const float py = p[t].y;
    const float pz = p[t].z;
    const float nx = n[t].x;
    const float ny = n[t].y;
    const float nz = n[t].z;

    matrix_A[rows][0] = nx;
    matrix_A[rows][1] = ny;
    matrix_A[rows][2] = nz;

    // compute dot product
    vector_B[rows] = (double)(px * nx + py * ny + pz * nz);

    ++rows;
  }

  // solve Ax=B using singular value decomposition
  QEF_evaluate(matrix_A, vector_B, rows, newPoint);
}

float voxel_density(const struct vector3f *v)
{
	//float vv = 0;
	const float s = 25.0f;
	//float vv = (float)pnoise(v->x/s, v->y/s, v->z/s);
	float vv = scaled_raw_noise_3d( 0, 1, v->x/s, v->y/s, v->z/s);
	//float vv = scaled_octave_noise_3d(  5, .25, 1, 0, 1, v->x/s, v->y/s, v->z/s);

	const float q = 200;
	float t = fabs(v->y);
	if (t<q) {
		float r =  1.0f - (t/q);
		vv*=r;
	} else {
		vv=0;
	}

	//float mm = 250;
	//float vv = scaled_octave_noise_2d(  5, .25, 1, 0, mm, v->x/s, v->z/s);
	//if (v->y<vv) vv = 0; else vv = 1;

	vv = 1.0 - vv;

	/*
	const struct vector3f z = { 0, 0, -70 };
	float l = vector3f_distance(&z, v);
	static float r = 128.0;
	if (l>(r/2) && l<r) {
		float value = (l/r);
		return value;
	} else {
		return 0;
	}
	*/

	return vv;

	/*
	const struct vector3f z = { 0, 0, -70 };
	float l = vector3f_distance(&z, v);
	static float r = 1024.0;
	if (l>(r/2) && l<r) {
		//float value = 1.0 - (l/r);
		//vv -= value;
	} else {
		if (l<(r/4)) return 0;
		vv = 1.0;
		return 1.0-vv;
	}

	//vv += (v->y/200);
	//vv += (v->y/50);

	vv+=BOUNDARY;

	if (vv<0) vv = 0;
	if (vv>1) vv = 1;

	return 1.0-vv;
	*/
}

#define EPSILON_NORMAL 1.0f
struct vector3f voxel_normal(const float d, const struct vector3f *v) {
	const struct vector3f v1 = { v->x+EPSILON_NORMAL, v->y, v->z };
	float dx = voxel_density(&v1) - d;
	const struct vector3f v2 = { v->x, v->y+EPSILON_NORMAL, v->z };
	float dy = voxel_density(&v2) - d;
	const struct vector3f v3 = { v->x, v->y, v->z+EPSILON_NORMAL };
	float dz = voxel_density(&v3) - d;

	const float l = sqrt( (dx*dx)+(dy*dy)+(dz*dz) );
	if (l==0) {
		return (struct vector3f) {0,0,0};
	}

	//return (struct vector3f) {dx/l, dy/l, dz/l};
	const struct vector3f value = {dx/l, dy/l, dz/l};

	return value;
}

struct vector3f get_normal(const struct vector3f *v) {
	float d = voxel_density(v);
	return voxel_normal(d, v);
}

static inline struct vector3b get_normal3(float d0, float dx, float dy, float dz) {
	float v1[] = { dx, dy, dz, 0 };
	__m128 v_a = _mm_load_ps(v1);     // Load dx,dy,dz

	float v2[] = { d0, d0, d0, 0 };
	__m128 v_b = _mm_load_ps(v2);    // Load d0,d0,d0

	__m128 s = _mm_sub_ps(v_a, v_b);  // dx*d0, dy*d0, dz*d0
	_mm_store_ps(v1, _mm_mul_ps(s, _mm_rsqrt_ps(_mm_dp_ps(s, s, 0x77))));

	return (struct vector3b) {v1[0]*127, v1[1]*127, v1[2]*127};
}


static inline uint8_t clamp(float value) {
	value*=255;
	if (value<0) value = 0;
	if (value>255) value = 255;
	return (uint8_t)value;
}

struct color4c get_color(const struct vector3s *v) {
	/*
	struct vector3f temp1 = { v->x+10, v->y-50, v->z+100 };
	struct vector3f temp2 = { v->x*2, v->y*2, v->z*2 };
	struct vector3f temp3 = { v->x/2, v->y/2, v->z/2 };
	float r= voxel_density(&temp1);
	float g= voxel_density(&temp2);
	float b= voxel_density(&temp3);
	return (struct color4c) { clamp(r), clamp(g), clamp(b) , 255 };
	*/
	return (struct color4c) { 127,127,127 , 255 };
}

static inline float _lerp_float(const float v0, const float v1, const uint8_t t) {
    return (v0*(255-t)+v1*t)/255.0;
}
static inline struct vector3f _lerp_vector3f(const struct vector3f *v1, const struct vector3f *v2, const uint8_t t) {
  struct vector3f temp = {
    _lerp_float(v1->x, v2->x, t),
    _lerp_float(v1->y, v2->y, t),
    _lerp_float(v1->z, v2->z, t),
  };
  return temp;
}

void error(const char* str) {
  printf("%s\n", str);
  exit(-1);
}

struct slice {
  struct vector3f *points;
  struct voxel *voxels;
  struct vector3s *qefs;
  int32_t z;
  bool empty;
  bool qefs_generated;
};

struct slice* _slice_create(int _xsize, int _ysize) {
	struct slice *s = malloc(sizeof(struct slice));
	s->points = malloc(sizeof(struct vector3f)*(_xsize+1)*(_ysize+1));
	s->voxels = malloc(sizeof(struct voxel)*(_xsize+1)*(_ysize+1));
	s->qefs = malloc(sizeof(struct vector3s)*(_xsize+1)*(_ysize+1));

	return s;
}
void _slice_free(struct slice* s) {
	free(s->points);
	free(s->voxels);
	free(s->qefs);
	free(s);
}

#define XY_INDEX(x,y,xsize) (((y)*xsize)+(x))

inline struct voxel get_voxel(const struct vector3f *v) {
	return (struct voxel) {
		.density = (uint8_t)(voxel_density(v)*255),
		.material_id = 1,
		.normal = (struct vector3b){0,0,0}
	};
}

void _proc_slice(struct slice* s, int z, const struct vector3i *min, const int xsize, const int ysize) {
	s->z = z;
	size_t ptr = 0;

	bool first;
	s->empty = true;
	s->qefs_generated = false;
	for (int32_t y = 0; y != ysize; y++) {
		for (int32_t x = 0; x != xsize; x++) {
			const struct vector3f vv = { x + min->x, y + min->y, z };
			s->points[ptr] = vv;
			s->voxels[ptr] = get_voxel(&vv);
			uint8_t density = s->voxels[ptr].density;
			if (x==0 && y==0) {
				first = density < BOUNDARY ? true : false;
			} else {
				bool temp = density < BOUNDARY ? true : false;
				if (temp!=first) s->empty = false;
			}

			ptr++;
		}
	}
}


void _generate_qefs(const struct vector3i *offset, struct slice* prev, struct slice* cur, const struct vector3i *min, const int xsize, const int ysize) {
	if (prev->qefs_generated) return;

	// Look at edges
	size_t ptr = 0;
	for (int32_t y = min->y, yy = 0; y != min->y+ysize; y++, yy++) {
		for (int32_t x = min->x, xx = 0; x != min->x+xsize; x++, xx++) {
			const struct vector3f v_aaa = prev->points[ptr];
			const struct vector3f v_baa = prev->points[ptr+1];
			const struct vector3f v_bba = prev->points[ptr+1+xsize];
			const struct vector3f v_aba = prev->points[ptr+xsize];
			const struct vector3f v_aab = cur->points[ptr];
			const struct vector3f v_bab = cur->points[ptr+1];
			const struct vector3f v_bbb = cur->points[ptr+1+xsize];
			const struct vector3f v_abb = cur->points[ptr+xsize];

			uint8_t aaa = prev->voxels[ptr].density;
			uint8_t baa = prev->voxels[ptr+1].density;
			uint8_t bba = prev->voxels[ptr+1+xsize].density;
			uint8_t aba = prev->voxels[ptr+xsize].density;
			uint8_t aab = cur->voxels[ptr].density;
			uint8_t bab = cur->voxels[ptr+1].density;
			uint8_t bbb = cur->voxels[ptr+1+xsize].density;
			uint8_t abb = cur->voxels[ptr+xsize].density;
			bool b_aaa = aaa < BOUNDARY ? true : false;
			bool b_baa = baa < BOUNDARY ? true : false;
			bool b_bba = bba < BOUNDARY ? true : false;
			bool b_aba = aba < BOUNDARY ? true : false;
			bool b_aab = aab < BOUNDARY ? true : false;
			bool b_bab = bab < BOUNDARY ? true : false;
			bool b_bbb = bbb < BOUNDARY ? true : false;
			bool b_abb = abb < BOUNDARY ? true : false;

			size_t num = 0;
			struct vector3f newPoint = { 0,0,0};
			if (b_aaa != b_aab) PROC2(v_aaa, v_aab, aaa, aab);
			if (b_baa != b_bab) PROC2(v_baa, v_bab, baa, bab);
			if (b_bba != b_bbb) PROC2(v_bba, v_bbb, bba, bbb);
			if (b_aba != b_abb) PROC2(v_aba, v_abb, aba, abb);
			if (b_aaa != b_baa) PROC2(v_aaa, v_baa, aaa, baa);
			if (b_aaa != b_aba) PROC2(v_aaa, v_aba, aaa, aba);
			if (b_bba != b_baa) PROC2(v_bba, v_baa, bba, baa);
			if (b_bba != b_aba) PROC2(v_bba, v_aba, bba, aba);
			if (b_aab != b_bab) PROC2(v_aab, v_bab, aab, bab);
			if (b_aab != b_abb) PROC2(v_aab, v_abb, aab, abb);
			if (b_bbb != b_bab) PROC2(v_bbb, v_bab, bbb, bab);
			if (b_bbb != b_abb) PROC2(v_bbb, v_abb, bbb, abb);

			if (num != 0) {
				newPoint.x /= num;
				newPoint.y /= num;
				newPoint.z /= num;

				const float scale = SCALE;
				prev->qefs[ptr] = (struct vector3s) {
					(int16_t)((newPoint.x-offset->x)*scale),
					(int16_t)((newPoint.y-offset->y)*scale),
					(int16_t)((newPoint.z-offset->z)*scale)
				};

				//printf("%d %d %d\n", prev->qefs[ptr].x, prev->qefs[ptr].y, prev->qefs[ptr].z);
			}

			ptr++;
		}
	}

	prev->qefs_generated = true;
}

void _proc_slice2(const struct vector3i *offset, struct slice* last, struct slice* prev, struct slice* cur, const struct vector3i *min, const int xsize, const int ysize, struct vertex* verts, size_t *num_tri) {

	_generate_qefs(offset, last,prev, min, xsize, ysize);
	_generate_qefs(offset, prev,cur, min, xsize, ysize);

	if (prev->empty && cur->empty) {
		return;
	}

	// z      prev
	// z - 1  last
	// z + 1 cur

	for (int32_t y=1;y<ysize-1;y++) {
		for (int32_t x=1;x<xsize-1;x++) {

			size_t ptr = y*xsize+x;

			uint8_t dn00 = prev->voxels[ptr-1].density; uint8_t d000 = prev->voxels[ptr].density; uint8_t dp00 = prev->voxels[ptr+1].density;
			uint8_t dnn0 = prev->voxels[ptr-1-xsize].density; uint8_t d0n0 = prev->voxels[ptr-xsize].density; uint8_t dpn0 = prev->voxels[ptr+1-xsize].density;
			uint8_t dnp0 = prev->voxels[ptr-1+xsize].density; uint8_t d0p0 = prev->voxels[ptr+xsize].density;
			uint8_t dn0n = last->voxels[ptr-1].density; uint8_t d00n = last->voxels[ptr].density; uint8_t dp0n = last->voxels[ptr+1].density;
			uint8_t d0nn = last->voxels[ptr-xsize].density; uint8_t dpnn = last->voxels[ptr+1-xsize].density;
			uint8_t dnpn = last->voxels[ptr-1+xsize].density; uint8_t d0pn = last->voxels[ptr+xsize].density;
			uint8_t dn0p = cur->voxels[ptr-1].density; uint8_t d00p = cur->voxels[ptr].density;
			uint8_t dnnp = cur->voxels[ptr-1-xsize].density; uint8_t d0np = cur->voxels[ptr-xsize].density;

#define IS_ZERO(a) a.x==0 && a.y==0 && a.z==0
			struct vector3b n000 = prev->voxels[ptr].normal; if (IS_ZERO(n000)) n000 = prev->voxels[ptr].normal = get_normal3(d000, dp00, d0p0, d00p);
			struct vector3b n0n0 = prev->voxels[ptr-xsize].normal; if (IS_ZERO(n0n0)) n0n0 = prev->voxels[ptr-xsize].normal = get_normal3(d0n0, dpn0, d000, d0np);
			struct vector3b n0nn = last->voxels[ptr-xsize].normal; if (IS_ZERO(n0nn)) n0nn = last->voxels[ptr-xsize].normal = get_normal3(d0nn, dpnn, d00n, d0n0);
			struct vector3b n00n = last->voxels[ptr].normal; if (IS_ZERO(n00n)) n00n = last->voxels[ptr].normal = get_normal3(d00n, dp0n, d0pn, d000);
			struct vector3b nn0n = last->voxels[ptr-1].normal; if (IS_ZERO(nn0n)) nn0n = last->voxels[ptr-1].normal = get_normal3(dn0n, d00n, dnpn, dn00);
			struct vector3b nn00 = prev->voxels[ptr-1].normal; if (IS_ZERO(nn00)) nn00 = prev->voxels[ptr-1].normal = get_normal3(dn00, d000, dnp0, dn0p);
			struct vector3b nnn0 = prev->voxels[ptr-1-xsize].normal; if (IS_ZERO(nnn0)) nnn0 = prev->voxels[ptr-1-xsize].normal = get_normal3(dnn0, d0n0, dn00, dnnp);

			bool val1 = prev->voxels[ptr].density < BOUNDARY ? true : false;
			{
				bool val2 = prev->voxels[ptr+1].density < BOUNDARY ? true : false;
				if (val1 != val2) { // Sign change, generate a quad
					struct vector3s va = prev->qefs[ptr];       // 000
					struct vector3s vb = prev->qefs[ptr-xsize]; // 0n0
					struct vector3s vc = last->qefs[ptr-xsize]; // 0nn
					struct vector3s vd = last->qefs[ptr];       // 00n

					const struct vertex a = { va, n000 , get_color(&va) };
					const struct vertex b = { vb, n0n0 , get_color(&vb) };
					const struct vertex c = { vc, n0nn , get_color(&vc) };
					const struct vertex d = { vd, n00n , get_color(&vd) };


					if (val1) {
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = b;
						verts[((*num_tri)*3)+2] = c;
						(*num_tri)++;
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = c;
						verts[((*num_tri)*3)+2] = d;
						(*num_tri)++;
					} else {
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = c;
						verts[((*num_tri)*3)+2] = b;
						(*num_tri)++;
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = d;
						verts[((*num_tri)*3)+2] = c;
						(*num_tri)++;
					}
				}
			}
			{
				bool val2 = prev->voxels[ptr+xsize].density < BOUNDARY ? true : false;
				if (val1 != val2) { // Sign change, generate a quad
					struct vector3s va = prev->qefs[ptr];   // 000
					struct vector3s vb = last->qefs[ptr];   // 00n
					struct vector3s vc = last->qefs[ptr-1]; // n0n
					struct vector3s vd = prev->qefs[ptr-1]; // n00

					const struct vertex a = { va, n000 , get_color(&va) };
					const struct vertex b = { vb, n00n , get_color(&vb) };
					const struct vertex c = { vc, nn0n , get_color(&vc) };
					const struct vertex d = { vd, nn00 , get_color(&vd) };

					if (val1) {
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = b;
						verts[((*num_tri)*3)+2] = c;
						(*num_tri)++;

						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = c;
						verts[((*num_tri)*3)+2] = d;
						(*num_tri)++;
					} else {
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = c;
						verts[((*num_tri)*3)+2] = b;
						(*num_tri)++;

						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = d;
						verts[((*num_tri)*3)+2] = c;
						(*num_tri)++;
					}
				}
			}
			{
				bool val2 = cur->voxels[ptr].density < BOUNDARY ? true : false;
				if (val1 != val2) { // Sign change, generate a quad
					struct vector3s va = prev->qefs[ptr];         // 000
					struct vector3s vb = prev->qefs[ptr-xsize];   // 0n0
					struct vector3s vc = prev->qefs[ptr-xsize-1]; // nn0
					struct vector3s vd = prev->qefs[ptr-1];       // n00

					const struct vertex a = { va, n000 , get_color(&va) };
					const struct vertex b = { vb, n0n0 , get_color(&vb) };
					const struct vertex c = { vc, nnn0 , get_color(&vc) };
					const struct vertex d = { vd, nn00 , get_color(&vd) };

					if (!val1) {
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = b;
						verts[((*num_tri)*3)+2] = c;
						(*num_tri)++;
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = c;
						verts[((*num_tri)*3)+2] = d;
						(*num_tri)++;
					} else {
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = c;
						verts[((*num_tri)*3)+2] = b;
						(*num_tri)++;
						verts[((*num_tri)*3)+0] = a;
						verts[((*num_tri)*3)+1] = d;
						verts[((*num_tri)*3)+2] = c;
						(*num_tri)++;
					}
				}
			}

		}
	}

}

struct mesh* voxel_generate_NEW(const struct vector3i *min, const struct vector3i *max) {
	int _xsize = max->x - min->x;
	int _ysize = max->y - min->y;
	int _zsize = max->z - min->z;

	struct slice *slices[3];
	for (size_t t=0; t!=3; t++) {
		slices[t] = _slice_create(_xsize, _ysize);
	}
	_proc_slice(slices[0], min->z, min, _xsize, _ysize);
	_proc_slice(slices[1], min->z+1, min, _xsize, _ysize);
	_proc_slice(slices[2], min->z+2, min, _xsize, _ysize);

	// Initial alloc
	size_t max_verts_per_level = _xsize*_ysize*6;
	size_t max_verts = max_verts_per_level*10;
	struct vertex* verts = (struct vertex*)malloc(sizeof(struct vertex)*max_verts);
	size_t num_tris = 0;

	struct slice* slice_a = slices[2];
	struct slice* slice_b = slices[1];
	struct slice* slice_c = slices[0];
	_proc_slice2(min, slice_c, slice_b, slice_a, min, _xsize, _ysize, verts, &num_tris);

	for (size_t z = 3; z != _zsize; z++) {
		struct slice* temp = slice_c;
		slice_c = slice_b;
		slice_b = slice_a;
		slice_a = temp;
		_proc_slice(slice_a, min->z+z, min, _xsize, _ysize);

		if (num_tris*3 > max_verts-max_verts_per_level) {
			printf("Realloc more verts %lu to %lu\n", max_verts, max_verts+max_verts_per_level);
			max_verts+=max_verts_per_level;
			verts = realloc(verts,sizeof(struct vertex)*max_verts);
		}

		_proc_slice2(min, slice_c, slice_b, slice_a, min, _xsize, _ysize, verts, &num_tris);
	}

	struct mesh* m = mesh_create(num_tris, verts);
	m->offset = (struct vector3f) { min->x, min->y, min->z };

	for (size_t t=0; t!=3; t++) {
		_slice_free(slices[t]);
	}

	return m;
}
