#include "main.h"
#include "mesh.h"
#include "engine.h"
#include "voxel.h"
#include "simplexnoise.h"
#include "camera.h"
#include "frustum.h"
#include "shader.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLFW/glfw3.h>
#include <locale.h>

#define _GNU_SOURCE
#include <pthread.h>

#define MAXM 16384

bool mouseToggle = true;

static void error_callback(int error, const char* description)
{
  fputs(description, stderr);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    glfwSetWindowShouldClose(window, GL_TRUE);
}

GLuint filter;                      // Which Filter To Use
GLuint fogMode[]= { GL_EXP, GL_EXP2, GL_LINEAR };   // Storage For Three Types Of Fog
GLuint fogfilter= 2;                    // Which Fog To Use
GLfloat fogColor[4]= {0.5f, 0.5f, 0.5f, 1.0f};      // Fog Color

void _setup_fog() {
	glClearColor(0.5f,0.5f,0.5f,1.0f);          // We'll Clear To The Color Of The Fog ( Modified )

	glFogi(GL_FOG_MODE, fogMode[fogfilter]);        // Fog Mode
	glFogfv(GL_FOG_COLOR, fogColor);            // Set Fog Color
	glFogf(GL_FOG_DENSITY, 0.05f);              // How Dense Will The Fog Be
	glHint(GL_FOG_HINT, GL_DONT_CARE);          // Fog Hint Value
	//glFogf(GL_FOG_START, MIN_DISTANCE);             // Fog Start Depth
	//glFogf(GL_FOG_END, (MIN_DISTANCE+VIEW_DISTANCE)/2);
	glFogf(GL_FOG_START, (MIN_DISTANCE+VIEW_DISTANCE)*.5);             // Fog Start Depth
	glFogf(GL_FOG_END, (MIN_DISTANCE+VIEW_DISTANCE)*.75);
	glEnable(GL_FOG);                   // Enables GL_FOG
}


struct camera* c;

static GLuint g_program;
static GLuint g_programCameraPositionLocation;
static GLuint g_programLightPositionLocation;
static GLuint g_programLightColorLocation;

static float g_cameraPosition[3];

float lastX = 0, lastY = 0;
int width = 640,height = 480;
double _mouseX=320, _mouseY=240;
void cursor_callback(GLFWwindow *window, double mouseX, double mouseY) {
	//_mouseX = mouseX;
	//_mouseY = mouseY;
}

void cursor_enter(GLFWwindow *window, int value) {
	if (value == GL_TRUE) printf("Cursor enter\n"); else printf("Cursor exit\n");
}

void process_mouse(GLFWwindow *window, double deltaTime) {
    static const GLfloat vertMouseSensitivity  = 500.0f;
    static const GLfloat horizMouseSensitivity = 500.0f;

	glfwGetWindowSize(window, &width, &height);
	glfwGetCursorPos(window, &_mouseX, &_mouseY);

	double midWindowX = width/2.0f;
	double midWindowY = height/2.0f;

    double horizMovement = _mouseX - midWindowX;
    double vertMovement  = _mouseY - midWindowY;
    float dx = horizMovement / horizMouseSensitivity;
    float dy = vertMovement / vertMouseSensitivity;

	glfwSetCursorPos(window, midWindowX, midWindowY);

	camera_ChangeHeading(c, -dx);
	camera_ChangePitch(c, -dy);
}

void keyboard_callback(GLFWwindow*  window, int key, int scancode, int action, int mods) {
	if (key==67 && action == GLFW_PRESS) {
		mouseToggle = !mouseToggle;

		if (mouseToggle) {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
			printf("Mouse move on\n");
		} else {
			glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
			printf("Mouse move off\n");
		}
	}
	if (key==76 && action == GLFW_PRESS) {
		mesh_lines_toggle();
	}
}

size_t frames = 0;
double startTime = 0;
size_t tc = 0;
void sceneCycle(GLFWwindow *window, size_t triangles)
{
	tc+=triangles;
    static unsigned int prevTicks = 0;
    unsigned int ticks;
    float secondsElapsed;
    int i;

    /* calculate the number of seconds that have
     * passed since the last call to this function */
    if(prevTicks == 0)
        prevTicks = clock();
    ticks = clock();
    secondsElapsed = (float)(ticks - prevTicks) / CLOCKS_PER_SEC;
    prevTicks = ticks;

	if (mouseToggle)
		process_mouse(window, secondsElapsed);

	frames++;
	float curTime = glfwGetTime();
	float elapsed = curTime - startTime;
	if (elapsed > 5) {
		printf("FPS: %4.1f  TPS: %'lu  - (frames:%zu  time:%f)\n", frames/elapsed, tc/frames, frames, elapsed);
		frames = 0;
		tc = 0;
		startTime = curTime;
	}
}


int main(void)
{

	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);

	printf("Create Window\n");
	window = glfwCreateWindow(1280, 768, "Simple example", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

    // obtain the existing locale name for numbers
    char *oldLocale = setlocale(LC_NUMERIC, NULL);

    // inherit locale from environment
    setlocale(LC_NUMERIC, "");


	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);
	//glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPos(window, 0, 0);
	glfwSetCursorPosCallback(window, cursor_callback);
	glfwSetCursorEnterCallback(window, cursor_enter);
	glfwSetKeyCallback(window, keyboard_callback);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glShadeModel(GL_SMOOTH);
	GLfloat global_ambient[] = { 0.5f, 0.5f, 0.5f, 1.0f };glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);
	GLfloat specular[] = {1.0f, 1.0f, 1.0f , 1.0f};glLightfv(GL_LIGHT0, GL_SPECULAR, specular);
	GLfloat lightpos[] = {0.5, 1., 1., 1}; glLightfv(GL_LIGHT0, GL_POSITION, lightpos);

	_setup_fog();

	glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ) ;
	glEnable ( GL_COLOR_MATERIAL ) ;

	printf("Start\n");
	struct engine e;
	engine_init(&e);

	struct vector3f camera_pos = {0,0,0};
	struct vector3f look_at = {1,0,0};
	struct vector3f up = {0,1,0};
	c = camera_create(&camera_pos, &look_at, &up, .1);

	printf("OpenGL Version: %s\n", glGetString(GL_VERSION));

	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float) height;
		glViewport(0, 0, width, height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		float s = .25f;
		glFrustum (-s*ratio, s, -s, s, 0.5, 2000.0);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		camera_Update(c);
		size_t triangles = engine_tick(&e, c);

		g_cameraPosition[0] = c->camera_pos.x;
		g_cameraPosition[1] = c->camera_pos.y;
		g_cameraPosition[2] = c->camera_pos.z;

		glfwSwapBuffers(window);
		glfwPollEvents();

		//float s = .1;
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) || GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT_SHIFT)) c->scale = .5; else c->scale = .1;
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W)) camera_Forward(c);
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S)) camera_Back(c);
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D)) camera_Right(c);
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A)) camera_Left(c);
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_Q)) camera_Up(c);
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_E)) camera_Down(c);
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_ESCAPE)) break;

		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_G)) {
			fogfilter=0;                   // Increase fogfilter By One
			glEnable(GL_FOG);                   // Enables GL_FOG
			glFogi (GL_FOG_MODE, fogMode[fogfilter]);   // Fog Mode
		}
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_H)) {
			fogfilter=1;                   // Increase fogfilter By One
			glEnable(GL_FOG);                   // Enables GL_FOG
			glFogi (GL_FOG_MODE, fogMode[fogfilter]);   // Fog Mode
		}
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_J)) {
			fogfilter=2;                   // Increase fogfilter By One
			glEnable(GL_FOG);                   // Enables GL_FOG
			glFogi (GL_FOG_MODE, fogMode[fogfilter]);   // Fog Mode
		}
		if ( GLFW_PRESS == glfwGetKey(window, GLFW_KEY_K)) {
			glDisable(GL_FOG);                   // Enables GL_FOG
		}

		float d = voxel_density(&c->camera_pos);
		if (d<=.51) {
			struct vector3f n = voxel_normal(d, &c->camera_pos);

			c->camera_pos = vector3f_add(&c->camera_pos, &n);
		}

		sceneCycle(window, triangles);
	}
	glfwDestroyWindow(window);
	glfwTerminate();


	//free(m);
    setlocale(LC_NUMERIC, oldLocale);

	exit(EXIT_SUCCESS);
}

