#include "mesh.h"
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include "vector.h"

bool lines_enabled = false;
void mesh_lines_toggle()
{
	lines_enabled = !lines_enabled;
}

void check() {
	GLenum value = glGetError();
	if (value == GL_NO_ERROR) return;

	printf("Error: %d\n", value);
	fflush(stdout);
	exit(-1);
}


static inline bool _vertex_equal(const struct vertex* a, const struct vertex* b)
{
	if (a==b) return true;
	if (a==NULL || b==NULL) return false;

	if (!vector3s_equal(&a->coord, &b->coord)) return false;
	if (!vector3b_equal(&a->normal, &b->normal)) return false;
	if (!_color4c_equal(&a->color, &b->color)) return false;

	return true;
}

static inline float _vertex_compare(const  struct vertex* a, const struct vertex* b)
{
	if (a==b) return 0;
	if (a==NULL || b==NULL) return NAN;

	float v;
	if ( (v = vector3s_compare(&a->coord, &b->coord)) != 0) return v;
	if ( (v = vector3b_compare(&a->normal, &b->normal)) != 0) return v;
	return _color4c_compare(&a->color, &b->color);
}

static inline size_t _search_binary(/*size_t* unique_index,*/ struct vertex* unique, size_t* num_verts, struct vertex* s)
{
	ssize_t lower = 0;
	ssize_t upper = *num_verts;
	ssize_t curIn = 0;

	while (lower <= upper)
	{
		curIn = (lower + upper ) / 2;

		float c = _vertex_compare(&unique[curIn], s);
		if(c < 0)
			lower = curIn + 1;
		else if(c > 0)
			upper = curIn - 1;
		else {
			//return unique_index[curIn];
			return curIn;
			break;
		}
	}
	float c = _vertex_compare(&unique[curIn], s);
	if (c==0) return curIn;

	printf("Not found! c(%d %d %d) n(%f %f %f)\n", s->coord.x,s->coord.y, s->coord.z,s->normal.x/127.0f,s->normal.y/127.0f,s->normal.z/127.0f);
	exit(-1);
}

static inline size_t _search(struct vertex* unique, size_t* num_verts, struct vertex* s)
{
	for (size_t k = 0; k != *num_verts; k++) {
		if (_vertex_equal(s, unique+k)) {
			return k;
		}
	}
	unique[*num_verts] = *s;
	(*num_verts)++;
	return *num_verts - 1;
}

int _qsort_vertex_cmp(const void *a, const void *b)
{
	const struct vertex *v1 = (const struct vertex*)a;
	const struct vertex *v2 = (const struct vertex*)b;

	float c = _vertex_compare(v1,v2);
	if (c==0) return 0;
	if (c>0 && c<1) return 1;
	if (c<0 && c>-1) return -1;
	return (int)c;
}

struct mesh* mesh_create(size_t triangle_num, struct vertex* vertices)
{
	struct vertex* unique = malloc(sizeof(struct vertex)*(triangle_num*3));
	struct triangle* triangles = malloc((triangle_num+1)*sizeof(struct triangle));

	size_t num_verts = 0;

	// Create a sorted list of vertices
	struct vertex* sorted_vertices = malloc(sizeof(struct vertex)*(triangle_num*3));
	for (size_t t=0; t!= triangle_num*3; t++) {
		sorted_vertices[t] = vertices[t];
	}
	qsort(sorted_vertices, triangle_num*3, sizeof(struct vertex), _qsort_vertex_cmp);

	//Get unique list of vertices
	for (size_t t=0; t != triangle_num*3; t++) {
		if (t==0) {
			unique[num_verts++] = sorted_vertices[t];  // First one goes in the list
		} else {
			if (!_vertex_equal(unique+num_verts-1, sorted_vertices+t) ) {
				unique[num_verts++] = sorted_vertices[t];  // Each one different then the head of the list goes into the list
			}
		}
	}

	for (size_t t = 0; t != triangle_num; t++) {
		for (size_t j = 0; j != 3; j++)
		{
			struct vertex* s = vertices + ((t*3)+j);
			triangles[t].indices[j] = _search_binary(unique, &num_verts, s);
		}
	}

	const size_t mesh_size = sizeof(struct mesh) + (sizeof(struct vertex)*num_verts) + (sizeof(struct triangle)*triangle_num);
	struct mesh* mesh = malloc(mesh_size);

	mesh->vertex_num = num_verts;
	mesh->triangle_num = triangle_num;
	mesh->vertices = (struct vertex *) mesh->data;
	mesh->triangles = (struct triangle*) (mesh->data + (sizeof(struct vertex)*num_verts));

	memcpy(mesh->vertices, unique, sizeof(struct vertex)*num_verts);
	memcpy(mesh->triangles, triangles, sizeof(struct triangle)*triangle_num);

	free(unique);
	free(triangles);

	mesh->bound = false;

	return mesh;
}

void mesh_erase(struct mesh* mesh) {
	if (mesh==NULL) return;

	//printf("Delete %p (%u %u)\n", mesh, mesh->vbo, mesh->ibo);fflush(stdout);
	if (mesh->bound) {
		glDeleteBuffers(1, &mesh->vbo); // de-Allocate and assign three Vertex Buffer Objects to our handle
		glDeleteBuffers(1, &mesh->ibo); // de-Allocate and assign three Vertex Buffer Objects to our handle
	}
	free(mesh);
}

void mesh_info(const struct mesh* m)
{
  printf("Vertices: %lu    Triangles: %lu\n", m->vertex_num, m->triangle_num);
}

void mesh_bind(struct mesh* mesh)
{
	glGenBuffers(1, &mesh->vbo); // Allocate and assign three Vertex Buffer Objects to our handle
	glGenBuffers(1, &mesh->ibo); // Allocate and assign three Vertex Buffer Objects to our handle

	// --------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo); // Bind our first VBO as being the active buffer and storing vertex attributes (coordinates)
	glBufferData(GL_ARRAY_BUFFER, mesh->vertex_num * sizeof(struct vertex), mesh->vertices, GL_STATIC_DRAW); // Copy the vertex data from diamond to our buffer

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->ibo); // Bind our first VBO as being the active buffer and storing vertex attributes (coordinates)
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, mesh->triangle_num * 3 * sizeof(int), mesh->triangles, GL_STATIC_DRAW); // Copy the vertex data from diamond to our buffer

	mesh->bound = true;
}

size_t mesh_draw(struct mesh* mesh)
{
	if (mesh==NULL) return 0;

	if (!mesh->bound) {
		mesh_bind(mesh);
	}

	const float scale = SCALE;
	glTranslatef(mesh->offset.x, mesh->offset.y, mesh->offset.z);
	glScalef(1.0f/scale,1.0f/scale,1.0f/scale);

	glBindBuffer(GL_ARRAY_BUFFER, mesh->vbo); // Bind our first VBO as being the active buffer and storing vertex attributes (coordinates)

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_SHORT, sizeof(struct vertex), 0);

	glEnableClientState(GL_NORMAL_ARRAY);
	glNormalPointer(GL_BYTE, sizeof(struct vertex), (void*)(sizeof(struct vector3s)));

	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(struct vertex), (void*)(sizeof(struct vector3s)+sizeof(struct vector3b)));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->ibo); // Bind our first VBO as being the active buffer and storing vertex attributes (coordinates)
	if (lines_enabled) {
		glDrawElements(	GL_LINES, mesh->triangle_num*3, GL_UNSIGNED_INT, 0);
	} else {
		glDrawElements(	GL_TRIANGLES, mesh->triangle_num*3, GL_UNSIGNED_INT, 0);
	}
	glScalef(scale,scale,scale);
	glTranslatef(-mesh->offset.x, -mesh->offset.y, -mesh->offset.z);

	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_NORMAL_ARRAY);

	check();

	return mesh->triangle_num;
}
