#ifndef __CAMERA__
#define __CAMERA__

#include "main.h"
#include "mesh.h"

struct camera {
        float max_pitch_rate, max_heading_rate;
        struct vector3f camera_pos, look_at, camera_up;
        float camera_heading, camera_pitch, scale;
        struct vector3f dir, mouse_pos, camera_pos_delta;
};

struct camera* camera_create(struct vector3f *pos, struct vector3f *lookat, struct vector3f *up, float viewscale);
void camera_ChangePitch(struct camera* c, float degrees);
void camera_ChangeHeading(struct camera* c, float degrees);
void camera_Move2D(struct camera* c, int x, int y);
void camera_SetPos(struct camera* c, int button, int state, int x, int y);
void camera_Update(struct camera* c);
void camera_Forward(struct camera* c);
void camera_Back(struct camera* c);
void camera_Right(struct camera* c);
void camera_Left(struct camera* c);
void camera_Up(struct camera* c);
void camera_Down(struct camera* c);

#endif // __CAMERA__
