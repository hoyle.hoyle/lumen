#ifndef __VOXEL
#define __VOXEL

struct __attribute__((__packed__)) voxel {
	struct vector3b normal;
	uint16_t material_id;
	uint8_t density;
};

struct mesh* voxel_generate();
struct mesh* voxel_generate_NEW(const struct vector3i *min, const struct vector3i *max);
float voxel_density(const struct vector3f *v);
struct vector3f voxel_normal(const float d, const struct vector3f *v);


#endif
