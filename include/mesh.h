#ifndef __MESH__
#define __MESH__

#include "main.h"
#include "vector.h"
#include <OpenGL/gl.h>

struct __attribute__((__packed__)) vertex {
  struct vector3s coord;
  struct vector3b normal;
  struct color4c color;
};

struct triangle {
  uint32_t indices[3];
};

struct mesh {
  bool bound;
  size_t vertex_num;
  struct vector3f offset;
  struct vertex *vertices;
  size_t triangle_num;
  struct triangle *triangles;
  GLuint vbo, ibo;
  uint8_t data[];
};

/* Take a set of vertices and remove uniques and create a optimized mesh */
struct mesh* mesh_create(size_t triangle_num, struct vertex* vertices);
size_t mesh_draw(struct mesh* mesh);
void mesh_info(const struct mesh* mesh);
void mesh_erase(struct mesh* mesh);
void mesh_lines_toggle();






#endif
