//----------------------------------------------------------------------------
// ThreeD Quadric Error Function
//----------------------------------------------------------------------------

#ifndef __QEF
#define __QEF

void QEF_evaluate( double mat[][3], double *vec, int rows, struct vector3f *point);

#endif 
