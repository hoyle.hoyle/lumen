#ifndef __VECTOR__
#define __VECTOR__

#include "main.h"

struct __attribute__((__packed__)) vector3f {
    float x;
    float y;
    float z;
};
struct __attribute__((__packed__)) vector4f {
    float w;
    float x;
    float y;
    float z;
};
struct __attribute__((__packed__)) vector3i {
    int32_t x;
    int32_t y;
    int32_t z;
};
struct __attribute__((__packed__)) vector3b {
    int8_t x;
    int8_t y;
    int8_t z;
};
struct __attribute__((__packed__)) vector3s {
    int16_t x;
    int16_t y;
    int16_t z;
};
struct __attribute__((__packed__)) color4c {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;
};

bool _color4c_equal(const struct color4c *a, const struct color4c* b);
float _color4c_compare(const struct color4c *a, const struct color4c* b);
bool _vector3f_equal(const struct vector3f* a, const struct vector3f* b);
float _vector3f_compare(const struct vector3f* a, const struct vector3f* b);

float vector3i_distance(const struct vector3i *a, const struct vector3i *b);
float vector3f_distance(const struct vector3f *a, const struct vector3f *b);
bool vector3i_equal(const struct vector3i *a, const struct vector3i *b);
struct vector3i vector3i_add(const struct vector3i *rhs, const struct vector3i *lhs);
struct vector3f vector3f_add(const struct vector3f *rhs, const struct vector3f *lhs);
struct vector3f vector3f_subtract(const struct vector3f *rhs, const struct vector3f *lhs);
void vector3f_add_assign(struct vector3f *rhs, const struct vector3f *lhs);
void vector3f_subtract_assign(struct vector3f *rhs, const struct vector3f *lhs);
struct vector3f vector3f_multiply(const struct vector3f *rhs, const struct vector3f *lhs);
struct vector3f vector3f_multiply_value(const struct vector3f *rhs, const float lhs);
struct vector3f vector3f_cross(struct vector3f *a, struct vector3f *b);
struct vector4f Q_from_AngAxis(float angle, struct vector3f *axis);
struct vector4f vector4f_normalize(const struct vector4f *a);
struct vector3f vector3f_normalize(const struct vector3f *a);
struct vector4f vector4f_inv(const struct vector4f *a);
struct vector4f vector4f_mult(const struct vector4f *a, const struct vector4f *b);
struct vector3f quatRotate(const struct vector3f *v, const struct vector4f *q);
struct vector4f vector4f_mod(const struct vector4f *rhs, const struct vector4f *lhs);

bool vector3b_equal(const struct vector3b* a, const struct vector3b* b);
float vector3b_compare(const struct vector3b* a, const struct vector3b* b);
bool vector3s_equal(const struct vector3s* a, const struct vector3s* b);
float vector3s_compare(const struct vector3s* a, const struct vector3s* b);
#endif // __VECTOR__
