#ifndef __FRUSTUM__
#define __FRUSTUM__

#include "main.h"

struct frustum {
	float planes[6][4];
};

void frustum_Extract(struct frustum *f);
bool frustum_PointInFrustum( const struct frustum *f, float x, float y, float z );
bool frustum_SphereInFrustum(const struct frustum *f,  float x, float y, float z, float radius );
float frustum_SphereInFrustum2(const struct frustum *f,  float x, float y, float z, float radius );
bool frustum_CubeInFrustum(const struct frustum *f,  float x, float y, float z, float size );

#endif // __FRUSTUM__
