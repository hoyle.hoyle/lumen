#pragma once

#include "main.h"
#include "vector.h"
#include "frustum.h"

#define _GNU_SOURCE
#include <pthread.h>


struct camera;

struct transfer_cmd {
	uint8_t type;
	void* data;
};

struct transfer_list {
	pthread_mutex_t lock;
	volatile size_t item_count;
	volatile size_t item_size;
	struct transfer_cmd *items;
};

struct block {
	int32_t state;
	struct vector3i coord;
	struct mesh* mesh;
};


struct engine {
	struct vector3i center;
	struct frustum frustum;

	size_t thread_count;
	struct transfer_list transfer_to_worker_threads[THREAD_COUNT];
	struct transfer_list transfer_from_worker_threads;

	size_t block_count;
	struct block* blocks;

	size_t to_generate_count;
	struct vector3i *to_generate_list;
};


void engine_init(struct engine *e);
size_t engine_tick(struct engine* e, struct camera *c);

static inline void transfer_list_init(struct transfer_list *bl) {
	if (0 != pthread_mutex_init(&bl->lock, NULL)) {
		printf("Error initializing mutex\n");
		exit(-1);
	}

	bl->item_count = 0;
	bl->item_size = 0;
	bl->items = NULL;
}

static inline void transfer_list_lock(struct transfer_list *bl) {
	if (bl==NULL) { printf("transfer list was null\n"); exit(-1); }
	pthread_mutex_lock(&bl->lock);
}

static inline void transfer_list_unlock(struct transfer_list *bl) {
	if (bl==NULL) { printf("transfer list was null\n"); exit(-1); }
	pthread_mutex_unlock(&bl->lock);
}

static inline void transfer_list_add(struct transfer_list *bl, struct transfer_cmd *cmd) {
	if (bl==NULL) { printf("transfer list was null\n"); exit(-1); }
	if (bl->item_count+1 >= bl->item_size) {
		bl->item_size += 1;
		bl->item_size *= 2;
		bl->items = realloc( bl->items, sizeof(struct transfer_cmd) * bl->item_size );
	}
	bl->items[bl->item_count++] = *cmd;
}

static inline void transfer_list_read(struct transfer_list *bl, size_t *read_amount, struct transfer_cmd **items) {
	if (bl==NULL) { printf("transfer list was null\n"); exit(-1); }
	*read_amount = bl->item_count;
	*items = bl->items;

	bl->items = NULL;
	bl->item_size = 0;
	bl->item_count = 0;
}


