#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include "khash.h"
#include <x86intrin.h>

#define VIEW_DISTANCE 384
#define MIN_DISTANCE 128
#define THREAD_COUNT 8
#define CHUNK_SIZE 64
#define SCALE 64
